$(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('.carousel').carousel({
          interval: 3000
        });
        $('#staticBackdrop').on('show.bs.modal',function (e){
            console.log("Se abrio un modal",e);
            $('#btnCompra').removeClass('btn-dark');
            $('#btnCompra').addClass('btn-primary');

        });
        $('#staticBackdrop').on('hidden.bs.modal',function (e){
            console.log("Se cerro un modal",e);
            $('#btnCompra').addClass('btn-dark');
        });

      });